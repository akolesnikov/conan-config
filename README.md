# Conan-config

## Introduction

This repo contains config files for conan: profiles and remote.txt.
For install these file and config your conan client run

> ``` conan config install --type git https://invent.kde.org/akolesnikov/conan-config.git```


More information about this you can find [here](https://docs.conan.io/en/latest/reference/commands/consumer/config.html#conan-config-install)
